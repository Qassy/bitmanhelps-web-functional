﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class backend2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerID",
                table: "ModelPanelCredentials",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerID",
                table: "ModelPanelCredentials");
        }
    }
}
