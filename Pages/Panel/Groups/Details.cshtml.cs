﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;

namespace bitmanhelps_web_functional.Pages.Panel.Groups
{
    public class DetailsModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public DetailsModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        public ModelPanelGroups ModelPanelGroups { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ModelPanelGroups = await _context.ModelPanelGroups.FirstOrDefaultAsync(m => m.GroupId == id);

            if (ModelPanelGroups == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
