﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace bitmanhelps_web_functional.Models
{
    public class ModelPayments
    {
        [Key]

        [Display(Name = "First Name")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        [Required]
        [StringLength(60)]
        public string FristName { get; set; }

        [Display(Name = "Last Name")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        [Required]
        [StringLength(60)]
        public string LastName { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Address Line 1")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string AddressPrimary { get; set; }

        [Display(Name = "Address Line 2 (Optional)")]
        public string AddressSecondary { get; set; }

        [Display(Name = "Postal Code")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        [Display(Name = "Card Name")]
        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string CardName { get; set; }

        [Display(Name = "Card Number")]
        [RegularExpression("[0-9]+")]
        [StringLength(16, MinimumLength = 16)]
        [Required]
        public int CardNumber { get; set; }

        [Display(Name = "Expiration Date")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime ExpDate { get; set; }

        [RegularExpression("[0-9]+")]
        [StringLength(3, MinimumLength = 3)]
        [Required]
        public int CVV { get; set; }


    }
}
