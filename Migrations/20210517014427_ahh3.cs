﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class ahh3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GroupId1",
                table: "ModelPanelCredentials",
                newName: "GroupId");
            migrationBuilder.RenameColumn(
                name: "UserIdId",
                table: "ModelPanelCredentials",
                newName: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
