﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class dropbadtables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
    name: "ModelPanelCred_Assignments");

            migrationBuilder.DropTable(
                name: "ModelPanelUser");

            migrationBuilder.DropTable(
                name: "ModelPanelUser_Assignments");

            migrationBuilder.DropTable(
                name: "Customers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModelPanelCred_Assignments");

            migrationBuilder.DropTable(
                name: "ModelPanelUser");

            migrationBuilder.DropTable(
                name: "ModelPanelUser_Assignments");

            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}
