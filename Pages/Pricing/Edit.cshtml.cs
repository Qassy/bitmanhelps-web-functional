﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;
using System.Security.Claims;


namespace bitmanhelps_web_functional.Pages.Pricing
{
    public class EditModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public EditModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ModelServices ModelServices { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId != "16506101-9756-430f-9f4e-aceb90b2af4a")
            {
                return Redirect("~/Identity/Account/Login");
            }

            if (id == null)
            {
                return NotFound();
            }

            ModelServices = await _context.ModelServices.FirstOrDefaultAsync(m => m.ServiceId == id);

            if (ModelServices == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ModelServices).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelServicesExists(ModelServices.ServiceId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ModelServicesExists(int id)
        {
            return _context.ModelServices.Any(e => e.ServiceId == id);
        }
    }
}
