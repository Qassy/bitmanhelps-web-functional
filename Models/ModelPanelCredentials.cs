﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace bitmanhelps_web_functional.Models
{
    public class ModelPanelCredentials
    {
        [Key]
        public int CredId { get; set; }

        public virtual ModelPanelGroups GroupId { get; set; }

        public virtual IdentityUser UserId { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Application { get; set; }

        [StringLength(60, MinimumLength = 3)]
        [Required]
        public string Login { get; set; }

        [Display(Name = "Password")]
        [StringLength(120, MinimumLength = 3)]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
