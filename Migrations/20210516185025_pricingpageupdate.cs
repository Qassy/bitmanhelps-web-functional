﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class pricingpageupdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CredPerUser",
                table: "ModelServices");

            migrationBuilder.RenameColumn(
                name: "UserMin",
                table: "ModelServices",
                newName: "CredMin");

            migrationBuilder.RenameColumn(
                name: "UserMax",
                table: "ModelServices",
                newName: "CredMax");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CredMin",
                table: "ModelServices",
                newName: "UserMin");

            migrationBuilder.RenameColumn(
                name: "CredMax",
                table: "ModelServices",
                newName: "UserMax");

            migrationBuilder.AddColumn<int>(
                name: "CredPerUser",
                table: "ModelServices",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
