﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class e3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserIdId",
                table: "ModelPanelGroups",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserIdId",
                table: "ModelPanelCredentials",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups",
                column: "UserIdId");

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups",
                column: "UserIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropColumn(
                name: "UserIdId",
                table: "ModelPanelCredentials");
        }
    }
}
