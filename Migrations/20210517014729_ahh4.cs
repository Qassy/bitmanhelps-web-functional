﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class ahh4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ModelPanelGroups");

            migrationBuilder.AddColumn<string>(
                name: "UserIdId",
                table: "ModelPanelGroups",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups",
                column: "UserIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups",
                column: "UserIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropColumn(
                name: "UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ModelPanelGroups",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
