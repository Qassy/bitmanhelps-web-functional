﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class CredPerUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CredPerUser",
                table: "ModelServices",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CredPerUser",
                table: "ModelServices");
        }
    }
}
