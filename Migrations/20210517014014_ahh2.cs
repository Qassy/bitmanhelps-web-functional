﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class ahh2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ModelPanelCredentials");

            migrationBuilder.RenameColumn(
                name: "ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                newName: "GroupId1");

            migrationBuilder.RenameIndex(
                name: "IX_ModelPanelCredentials_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                newName: "IX_ModelPanelCredentials_GroupId1");

            migrationBuilder.AddColumn<string>(
                name: "UserIdId",
                table: "ModelPanelCredentials",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserIdId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1",
                table: "ModelPanelCredentials",
                column: "GroupId1",
                principalTable: "ModelPanelGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1",
                table: "ModelPanelCredentials");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "GroupId1",
                table: "ModelPanelCredentials",
                newName: "ModelPanelGroupsGroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ModelPanelCredentials_GroupId1",
                table: "ModelPanelCredentials",
                newName: "IX_ModelPanelCredentials_ModelPanelGroupsGroupId");

            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "ModelPanelCredentials",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "ModelPanelCredentials",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                column: "ModelPanelGroupsGroupId",
                principalTable: "ModelPanelGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
