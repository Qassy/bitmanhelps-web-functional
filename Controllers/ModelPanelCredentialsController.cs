﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;

namespace bitmanhelps_web_functional.Pages.Panel.Credentials
{
    public class ModelPanelCredentialsController : Controller
    {
        private readonly bitmanhelps_web_functionalContext _context;

        public ModelPanelCredentialsController(bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        // GET: ModelPanelCredentials
        public async Task<IActionResult> Index()
        {
            var credId = _context.ModelPanelCredentials
                 .Include(c => c.GroupId)
                 .AsNoTracking();
            return View(await _context.ModelPanelCredentials.ToListAsync());
        }

        // GET: ModelPanelCredentials/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelCredentials = await _context.ModelPanelCredentials
                .FirstOrDefaultAsync(m => m.CredId == id);
            if (modelPanelCredentials == null)
            {
                return NotFound();
            }

            return View(modelPanelCredentials);
        }

        // GET: ModelPanelCredentials/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ModelPanelCredentials/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CredId,Application,Login,Password")] ModelPanelCredentials modelPanelCredentials)
        {
            if (ModelState.IsValid)
            {
                _context.Add(modelPanelCredentials);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(modelPanelCredentials);
        }

        // GET: ModelPanelCredentials/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelCredentials = await _context.ModelPanelCredentials.FindAsync(id);
            if (modelPanelCredentials == null)
            {
                return NotFound();
            }
            return View(modelPanelCredentials);
        }

        // POST: ModelPanelCredentials/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CredId,Application,Login,Password")] ModelPanelCredentials modelPanelCredentials)
        {
            if (id != modelPanelCredentials.CredId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(modelPanelCredentials);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModelPanelCredentialsExists(modelPanelCredentials.CredId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(modelPanelCredentials);
        }

        // GET: ModelPanelCredentials/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelCredentials = await _context.ModelPanelCredentials
                .FirstOrDefaultAsync(m => m.CredId == id);
            if (modelPanelCredentials == null)
            {
                return NotFound();
            }

            return View(modelPanelCredentials);
        }

        // POST: ModelPanelCredentials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var modelPanelCredentials = await _context.ModelPanelCredentials.FindAsync(id);
            _context.ModelPanelCredentials.Remove(modelPanelCredentials);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModelPanelCredentialsExists(int id)
        {
            return _context.ModelPanelCredentials.Any(e => e.CredId == id);
        }
    }
}
