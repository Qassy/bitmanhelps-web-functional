﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace bitmanhelps_web_functional.Models
{
    public class ModelServices
    {
        [Key]
        public int ServiceId { get; set; }

        [Display(Name = "Service Name")]
        public string ServiceName { get; set; }

        [DataType(DataType.Currency)]
        public float Price { get; set; }

        [Display(Name = "Minimum Credentials")]
        public int CredMin { get; set; }

        [Display(Name = "Maximum Credentials")]
        public int CredMax { get; set; }

        [Display(Name = "Service Branding")]
        public string ServiceBranding { get; set; }
    }
}
