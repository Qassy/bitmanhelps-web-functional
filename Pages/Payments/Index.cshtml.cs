﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;


namespace bitmanhelps_web_functional.Pages.Payments
{
    public class IndexModel : PageModel
    {
        public string Country { get; set; }
        public string Province { get; set; }

        public List<SelectListItem> Countries { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "CA", Text = "Canada" },
            new SelectListItem { Value = "US", Text = "USA"  },
            new SelectListItem { Value = "MX", Text = "Mexico" },
            new SelectListItem { Value = "UK", Text = "United Kingdom" },
        };

        public List<SelectListItem> Provinces { get; } = new List<SelectListItem>
        {
            new SelectListItem { Value = "BC", Text = "British Columbia" },
            new SelectListItem { Value = "AB", Text = "Alberta"  },
        };

        public void OnGet()
        {
        }
    }
}
