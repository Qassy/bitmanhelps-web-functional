﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class updatecreds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnrollmentDate",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "ModelPanelCredentials");

            migrationBuilder.RenameColumn(
                name: "CredID",
                table: "ModelPanelCredentials",
                newName: "CredId");

            migrationBuilder.RenameColumn(
                name: "CustomerID",
                table: "ModelPanelCredentials",
                newName: "UserId");

            migrationBuilder.AddColumn<int>(
                name: "GroupId",
                table: "ModelPanelCredentials",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.RenameColumn(
                name: "CredId",
                table: "ModelPanelCredentials",
                newName: "CredID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ModelPanelCredentials",
                newName: "CustomerID");

            migrationBuilder.AddColumn<DateTime>(
                name: "EnrollmentDate",
                table: "ModelPanelCredentials",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "ModelPanelCredentials",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
