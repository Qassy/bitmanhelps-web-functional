﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;
using System.Security.Claims;


namespace bitmanhelps_web_functional.Pages.Pricing
{
    public class IndexModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public IndexModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        public IList<ModelServices> ModelServices { get;set; }

        public async Task OnGetAsync()
        {
            ModelServices = await _context.ModelServices.ToListAsync();
        }
    }
}
