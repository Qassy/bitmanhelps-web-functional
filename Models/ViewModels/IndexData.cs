﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace bitmanhelps_web_functional.Models.ViewModels
{
    public class IndexData
    {
        public IEnumerable<ModelPanelGroups> ModelPanelGroups { get; set; }
        public IEnumerable<ModelPanelCredentials> ModelPanelCredentials { get; set; }

    }
}
