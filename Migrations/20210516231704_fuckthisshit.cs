﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class fuckthisshit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnrollmentDate",
                table: "ModelPanelGroups");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "ModelPanelGroups");

            migrationBuilder.RenameColumn(
                name: "GroupID",
                table: "ModelPanelGroups",
                newName: "GroupId");

            migrationBuilder.RenameColumn(
                name: "CustomerID",
                table: "ModelPanelGroups",
                newName: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GroupId",
                table: "ModelPanelGroups",
                newName: "GroupID");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "ModelPanelGroups",
                newName: "CustomerID");

            migrationBuilder.AddColumn<DateTime>(
                name: "EnrollmentDate",
                table: "ModelPanelGroups",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "ModelPanelGroups",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
