﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace bitmanhelps_web_functional.Models
{
    public class ModelPanelGroups
    {
        [Key]
        public int GroupId { get; set; }

        public virtual IdentityUser UserId { get; set; }

        [Display(Name = "Group Name")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        [Required]
        [StringLength(60)]
        public string GroupName { get; set; } 

    }
}
