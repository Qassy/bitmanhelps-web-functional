﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;

namespace bitmanhelps_web_functional.Pages.Panel.Credentials
{
    public class DeleteModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public DeleteModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ModelPanelCredentials ModelPanelCredentials { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ModelPanelCredentials = await _context.ModelPanelCredentials.FirstOrDefaultAsync(m => m.CredId == id);

            if (ModelPanelCredentials == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ModelPanelCredentials = await _context.ModelPanelCredentials.FindAsync(id);

            if (ModelPanelCredentials != null)
            {
                _context.ModelPanelCredentials.Remove(ModelPanelCredentials);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
