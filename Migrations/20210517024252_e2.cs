﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class e2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1GroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.RenameColumn(
                name: "GroupId1GroupId",
                table: "ModelPanelCredentials",
                newName: "GroupId1");

            migrationBuilder.RenameIndex(
                name: "IX_ModelPanelCredentials_GroupId1GroupId",
                table: "ModelPanelCredentials",
                newName: "IX_ModelPanelCredentials_GroupId1");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1",
                table: "ModelPanelCredentials",
                column: "GroupId1",
                principalTable: "ModelPanelGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1",
                table: "ModelPanelCredentials");

            migrationBuilder.RenameColumn(
                name: "GroupId1",
                table: "ModelPanelCredentials",
                newName: "GroupId1GroupId");

            migrationBuilder.RenameIndex(
                name: "IX_ModelPanelCredentials_GroupId1",
                table: "ModelPanelCredentials",
                newName: "IX_ModelPanelCredentials_GroupId1GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_GroupId1GroupId",
                table: "ModelPanelCredentials",
                column: "GroupId1GroupId",
                principalTable: "ModelPanelGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
