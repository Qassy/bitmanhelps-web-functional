﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class ahh1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelCredentials_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                column: "ModelPanelGroupsGroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials",
                column: "ModelPanelGroupsGroupId",
                principalTable: "ModelPanelGroups",
                principalColumn: "GroupId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_ModelPanelGroups_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelCredentials_ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "ModelPanelGroupsGroupId",
                table: "ModelPanelCredentials");
        }
    }
}
