﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;
using System.Security.Claims;


namespace bitmanhelps_web_functional.Pages.Pricing
{
    public class CreateModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public CreateModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ModelServices ModelServices { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId != "16506101-9756-430f-9f4e-aceb90b2af4a")
            {
                return Redirect("~/Identity/Account/Login");
            }

            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.ModelServices.Add(ModelServices);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
