﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;
using bitmanhelps_web_functional.Models.ViewModels;

namespace bitmanhelps_web_functional.Pages.Panel.Groups
{
    public class IndexModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public IndexModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        public IndexData indexData { get; set; }
        public int GroupId { get; set; }
        public int UserId { get; set; }

        public IList<ModelPanelGroups> ModelPanelGroups { get;set; }

        public async Task OnGetAsync(int? id, int? UserId)
        {
            ModelPanelGroups = await _context.ModelPanelGroups
                .Include(c => c.UserId)
                .AsNoTracking()
                .ToListAsync();
            }
        }
    }
