﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace bitmanhelps_web_functional.Migrations
{
    public partial class ahh7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups");

            migrationBuilder.DropIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ModelPanelGroups");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "ModelPanelCredentials");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "isAdmin",
                table: "AspNetUsers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "ModelPanelGroups",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "ModelPanelCredentials",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "isAdmin",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelGroups_UserIdId",
                table: "ModelPanelGroups",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ModelPanelCredentials_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelCredentials_AspNetUsers_UserIdId",
                table: "ModelPanelCredentials",
                column: "UserIdId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ModelPanelGroups_AspNetUsers_UserIdId",
                table: "ModelPanelGroups",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
