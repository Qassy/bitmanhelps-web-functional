﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace bitmanhelps_web_functional.Data
{
    public class bitmanhelps_web_functionalContext : IdentityDbContext
    {
        public bitmanhelps_web_functionalContext (DbContextOptions<bitmanhelps_web_functionalContext> options)
            : base(options)
        {
        }

        public DbSet<bitmanhelps_web_functional.Models.ModelPanelCredentials> ModelPanelCredentials { get; set; }

        public DbSet<bitmanhelps_web_functional.Models.ModelPanelGroups> ModelPanelGroups { get; set; }

        public DbSet<bitmanhelps_web_functional.Models.ModelServices> ModelServices { get; set; }

        public DbSet<bitmanhelps_web_functional.Models.ModelPanelAdminUsers> ModelPanelAdminUsers { get; set; }

    }
}
