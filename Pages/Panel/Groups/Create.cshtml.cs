﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;

namespace bitmanhelps_web_functional.Pages.Panel.Groups
{
    public class CreateModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public CreateModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ModelPanelGroups ModelPanelGroups { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.ModelPanelGroups.Add(ModelPanelGroups);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
