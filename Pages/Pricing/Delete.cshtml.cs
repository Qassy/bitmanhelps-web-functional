﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;
using System.Security.Claims;


namespace bitmanhelps_web_functional.Pages.Pricing
{
    public class DeleteModel : PageModel
    {
        private readonly bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext _context;

        public DeleteModel(bitmanhelps_web_functional.Data.bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ModelServices ModelServices { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            string userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            if (userId != "16506101-9756-430f-9f4e-aceb90b2af4a")
            {
                return Redirect("~/Identity/Account/Login");
            }

            if (id == null)
            {
                return NotFound();
            }

            ModelServices = await _context.ModelServices.FirstOrDefaultAsync(m => m.ServiceId == id);

            if (ModelServices == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ModelServices = await _context.ModelServices.FindAsync(id);

            if (ModelServices != null)
            {
                _context.ModelServices.Remove(ModelServices);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
