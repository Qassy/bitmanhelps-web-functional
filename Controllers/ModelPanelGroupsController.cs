﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using bitmanhelps_web_functional.Data;
using bitmanhelps_web_functional.Models;

namespace bitmanhelps_web_functional.Controllers
{
    public class ModelPanelGroupsController : Controller
    {
        private readonly bitmanhelps_web_functionalContext _context;

        public ModelPanelGroupsController(bitmanhelps_web_functionalContext context)
        {
            _context = context;
        }

        // GET: ModelPanelGroups
        public async Task<IActionResult> Index()
        {
            var credId = _context.ModelPanelCredentials
                 .Include(c => c.UserId)
                 .AsNoTracking();
            return View(await _context.ModelPanelGroups.ToListAsync());
        }

        // GET: ModelPanelGroups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelGroups = await _context.ModelPanelGroups
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (modelPanelGroups == null)
            {
                return NotFound();
            }

            return View(modelPanelGroups);
        }

        // GET: ModelPanelGroups/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ModelPanelGroups/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupId,GroupName")] ModelPanelGroups modelPanelGroups)
        {
            if (ModelState.IsValid)
            {
                _context.Add(modelPanelGroups);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(modelPanelGroups);
        }

        // GET: ModelPanelGroups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelGroups = await _context.ModelPanelGroups.FindAsync(id);
            if (modelPanelGroups == null)
            {
                return NotFound();
            }
            return View(modelPanelGroups);
        }

        // POST: ModelPanelGroups/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GroupId,GroupName")] ModelPanelGroups modelPanelGroups)
        {
            if (id != modelPanelGroups.GroupId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(modelPanelGroups);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ModelPanelGroupsExists(modelPanelGroups.GroupId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(modelPanelGroups);
        }

        // GET: ModelPanelGroups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var modelPanelGroups = await _context.ModelPanelGroups
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (modelPanelGroups == null)
            {
                return NotFound();
            }

            return View(modelPanelGroups);
        }

        // POST: ModelPanelGroups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var modelPanelGroups = await _context.ModelPanelGroups.FindAsync(id);
            _context.ModelPanelGroups.Remove(modelPanelGroups);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ModelPanelGroupsExists(int id)
        {
            return _context.ModelPanelGroups.Any(e => e.GroupId == id);
        }
    }
}
